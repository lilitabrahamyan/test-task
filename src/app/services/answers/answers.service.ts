import { Injectable } from '@angular/core';
import {ApiService} from '../api/api.service';
@Injectable({
  providedIn: 'root'
})
export class AnswersService {

  constructor(
    private readonly api: ApiService
  ) { }

  public saveAnswer(data){
    return this.api.request('post', 'answers', data);
  }
}
