import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(
    private readonly api: ApiService
  ) {
  }

  public getAllQuestions(fieldName= 'id', order= 'ASC') {

    return this.api.request('get', `questions?_sort=${fieldName}&_order=${order}`, null);
  }

  public addQuestion(data){

    return this.api.request('post', 'questions', data);
  }

  public updateQuestion(data){

    const obj = this.collectQuestionData(data);

    return this.api.request('patch', `questions/${obj.id}`, obj.data);
  }

  public deleteQuestion(id){
    return this.api.request('delete', `questions/${id}`, null);
  }

  public getQuestionWiltAnswer(fieldName= 'id', order= 'ASC'){
    return this.api.request('get', `questions?_sort=${fieldName}&_order=${order}_embed=questions&_embed=answers`, null);
  }

  private collectQuestionData(data){
    const  id = data.id;
    delete  data.id;
    return {
      id,
      data
    };
  }
}
