import {Component, OnDestroy, OnInit} from '@angular/core';
import {AnswersService} from '../../services/answers/answers.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {QuestionService} from '../../services/question/question.service';
import {QuestionModel} from '../../models/QuestionModel';
import { PrimeNGConfig } from 'primeng/api';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit, OnDestroy {

  public answerData: FormGroup = new FormGroup({
    open: this.fb.array([], [Validators.required, Validators.minLength(1), Validators.maxLength(255)]),
    singleSelect: this.fb.array([  ]),
    multiSelect: this.fb.array([ ]),
  });

  private unsubscribe$: Subject<void> = new Subject<void>();
  public notAnsweredQuestion: QuestionModel[] = [];
  public answeredQuestion: QuestionModel[] = [];
  public open: FormArray;
  public singleSelect: FormArray;
  public multiSelect: FormArray;
  constructor(
    private fb: FormBuilder,
    private primengConfig: PrimeNGConfig,
    private readonly answersService: AnswersService,
    private questionService: QuestionService
  ) { }

  ngOnInit(): void {
    this.getQuestionWiltAnswer();
  }

  private getQuestionWiltAnswer(){

    this.questionService.getQuestionWiltAnswer().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((res) => {
      this.answeredQuestion = [];
      this.notAnsweredQuestion = [];
      res.forEach((item, i) => {
        if (item.answers.length > 0){
          this.answeredQuestion.push(item);
        }else {
          this.notAnsweredQuestion.push(item);

        }
      });
      this.notAnsweredQuestion.forEach((item, index) => {
        if (item.questionType === 'Open'){
          this.open = this.answerData.get('open') as FormArray;
          // this.open[index] = this.createItem('open')
          this.open.push(this.createItem('open'));
        }
        if (item.questionType === 'SingleSelect'){
          this.singleSelect = this.answerData.get('singleSelect') as FormArray;
          item.items.forEach((single, i) => {
            this.singleSelect.push(this.createItem('singleSelect'));
          });

         // this.singleSelect.push(this.createItem('singleSelect'));
        }
        if (item.questionType === 'MultiSelect'){
          this.multiSelect = this.answerData.get('multiSelect') as FormArray;
          item.items.forEach((single, i) => {
            this.multiSelect.push(this.createItem('multiSelect'));
          });
        }
      });
    });
  }

  createItem(name): FormGroup {
    return this.fb.group({
      [name]: '',
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  saveAnswer(data) {
    this.answersService.saveAnswer(data).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((res) => {
      this.getQuestionWiltAnswer();
      this.answerData.reset();
    });
  }

  correctOpenData(index){
    this.saveAnswer({
      questionId: this.notAnsweredQuestion[index].id,
      answer: this.answerData.get('open').value[index].open,
      createdDate: new Date(),
    });
  }

  correctSingleData(index){
  let item;
  for (const single of this.answerData.get('singleSelect').value){
    if (single.singleSelect !== ''){
        item = single.singleSelect;
      }
    }
  const questionI = item.split('_')[0];
  const answerI = item.split('_')[1];
  console.log(questionI);
  console.log(index);
  if (questionI != index){return; }

  this.saveAnswer({
       questionId: this.notAnsweredQuestion[index].id,
       answer:  this.notAnsweredQuestion[index].items[answerI].variation,
       createdDate: new Date(),
     });
  }

  correctMultipleData(index){
    const arr = [];
    for (const single of this.answerData.get('multiSelect').value){
      console.log(single.multiSelect);
      if (single.multiSelect !== ''){
        arr.push(single.multiSelect[0]);
      }
    }

    for (const item of arr){
      const questionI = item.split('_')[0];
      const answerI = item.split('_')[1];
      console.log(questionI);
      console.log(index);
      if (questionI != index){continue; }

        this.saveAnswer({
          questionId: this.notAnsweredQuestion[index].id,
          answer:  this.notAnsweredQuestion[index].items[answerI].variation,
          createdDate: new Date(),
        });
    }


  }

  counter(i: number) {
    return new Array(i);
  }
}
