import {Component, OnDestroy, OnInit} from '@angular/core';
import {QuestionService} from '../../services/question/question.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {MessageService, PrimeNGConfig, ConfirmationService} from 'primeng/api';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  public questionDataGroup: FormGroup = new FormGroup({
    question: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(255)]),
    questionType: new FormControl('', [Validators.required])
  });

  public questionData;
  public questionType = [
    {
      label: 'MultiSelect',
      value: 'MultiSelect',
    },
    {
      label: 'SingleSelect',
      value: 'SingleSelect',
    },
    {
      label: 'Open',
      value: 'Open',
    },
  ];
  private unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private confirmationService: ConfirmationService,
    private primengConfig: PrimeNGConfig,
    private messageService: MessageService,
    private readonly questionService: QuestionService
  ) {
    this.primengConfig.ripple = true;
  }

  ngOnInit(): void {
    this.getAllQuestions();
  }

  private getAllQuestions() {
    this.questionService.getAllQuestions().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(res => {
      this.questionData = res;
    });
  }

  onRowEditInit(rowData: any) {
    console.log('asaqsas', rowData);
  }

  onRowEditSave(rowData: any) {
    console.log('fffffff', rowData);
    this.questionService.updateQuestion(rowData).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(res => {
      this.messageService.add({severity: 'success', summary: 'Success', detail: 'Successfully updated'});
    });
  }

  onRowEditCancel(rowData: any, ri: any) {

  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  deleteItemConfirmation(id) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete question?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deleteQuestion(id);
      }
    });
  }

  private deleteQuestion(id){
    this.questionService.deleteQuestion(id).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(res => {
      this.getAllQuestions();
      this.messageService.add({severity: 'success', summary: 'Success', detail: 'Successfully deleted'});
    });
  }
}
