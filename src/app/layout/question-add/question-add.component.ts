import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder, FormArray} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService, PrimeNGConfig} from 'primeng/api';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {QuestionService} from '../../services/question/question.service';

@Component({
  selector: 'app-question-add',
  templateUrl: './question-add.component.html',
  styleUrls: ['./question-add.component.scss']
})
export class QuestionAddComponent implements OnInit, OnDestroy {

  public questionData: FormGroup = new FormGroup({
    question: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(255)]),
    questionType: new FormControl('', [Validators.required]),
    items: this.fb.array([ this.createItem() ])
  });

  public questionType = [
    {
      name: 'MultiSelect',
      value: 'MultiSelect',
    },
    {
      name: 'SingleSelect',
      value: 'SingleSelect',
    },
    {
      name: 'Open',
      value: 'Open',
    },
  ];
  public icon = 'pi pi-times';
  public variations = false;
  public multi = false;
  public items: FormArray;

  private unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private questionService: QuestionService
  ) {
    this.primengConfig.ripple = true;
  }

  ngOnInit(): void {
    this.questionData.valueChanges
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe((d) => {
        if (this.questionData.valid) {
         this.icon = 'pi pi-check';
        } else {
          this.icon = 'pi pi-times';
        }
      });
  }

  save() {
    if (this.questionData.invalid) {
      this.questionData.markAllAsTouched();
      return false;
    }

    this.questionData.setValue({
      questionType: this.questionData.get('questionType').value.name,
      question:  this.questionData.get('question').value,
      items:  this.questionData.get('items').value,
    });
    if (this.questionData.get('questionType').value === 'Open'){
     this.questionData.removeControl('items');
    }


    this.questionData.addControl('createdDate',
      this.fb.control(new Date(), ));

    this.questionService.addQuestion(this.questionData.value).pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((res) => {
      console.log(res);
      this.router.navigate(['/']);
    });
  }


  typeChanged() {
    if (!this.questionData.get('items')){
      this.questionData.addControl('items', this.fb.array([ this.createItem() ]));
    }
    if (this.questionData.get('questionType').value.name === 'Open'){
      this.variations = false;
      return;
    }

    if(this.questionData.get('questionType').value.name === 'MultiSelect'){
      this.multi = true;
    }
    this.variations = true;
  }

  addVariations() {
    this.items = this.questionData.get('items') as FormArray;
    this.items.push(this.createItem());
    console.log('asas', this.questionData)
  }

  createItem(): FormGroup {
    return this.fb.group({
      variation: '',
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
