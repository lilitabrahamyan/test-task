import {QuestionTypeModel} from './QuestionTypeModel';
import {ItemsModel} from './ItemsModel';

export interface QuestionModel {
  id: number;
  question: string;
  questionType: string;
  items: ItemsModel[];
  createdDate: QuestionTypeModel;
}
