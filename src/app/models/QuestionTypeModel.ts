export interface QuestionTypeModel {
  SingleSelect: string;
  MultiSelect: string;
  Open: string;
}
